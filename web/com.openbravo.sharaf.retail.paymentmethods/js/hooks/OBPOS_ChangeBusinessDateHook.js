/**
 * 
 */

(function() {
    OB.UTIL.HookManager.registerHook('OBPOS_ChangeBusinessDateHook', function(args, callbacks) {
        OB.Dal.find(OB.Model.CashUp, {
            isprocessed: 'N'
        }, function(cashUp) {
            if (cashUp.length === 1 && OB.UTIL.isNullOrUndefined(cashUp.at(0).get('objToSend'))) {// to run ajax call only when cashup is completed and new cashup is not created
                console.log('********** invoked DeActivatePaymentMethods ajax call for deactivating payment methods after changinging businessdate **********')
                new OB.DS.Process('com.openbravo.sharaf.retail.paymentmethods.service.DeActivatePaymentMethods').exec({
                    terminalId: OB.MobileApp.model.get('terminal').id,
                    businessDate: moment(new Date()).format('YYYY-MM-DD') // since Business date will be set as current date
                }, function(data) {
                    if (data && data.exception) {
                        if (!OB.UTIL.isNullOrUndefined(data.exception.message)) {
                            console.log(data.exception.message);
                            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                        }
                    } else {
                        var paymentMethod;
                        var paymentArrFromAjax = [];
                        var paymentArray = [];
                        if (Object.keys(data).length > 0) {
                            for (var i = 0; i < Object.keys(data).length; i++) {
                                var p = Object.keys(data)[i];
                                paymentArrFromAjax.push(p);
                            }
                        }
                        var pIndex = 0;
                        OB.MobileApp.model.get('payments').forEach(function(pay) {
                            if (paymentArrFromAjax.indexOf(pay.paymentMethod.id) === -1) {
                                paymentArray[pIndex] = pay;
                                pIndex++;
                            }
                        });
                        OB.MobileApp.model.attributes.payments = paymentArray;
                        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                    }
                });
            } else {
            	//cashup is present already ... so, don't deactivate any payment
            	OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            }
        });
    });
}());