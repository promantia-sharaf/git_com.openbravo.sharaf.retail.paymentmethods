/**
 *
 */

(function() {
    OB.UTIL.HookManager.registerHook('OBPOS_TerminalLoadedFromBackend', function(args, callbacks) {
    	if(!OB.UTIL.isNullOrUndefined(OB.UTIL.localStorage.getItem('businessdate'))){
            OB.Dal.find(OB.Model.CashUp, {
                isprocessed: 'N'
            }, function(cashUp) {
                if (cashUp.length === 1 && OB.UTIL.isNullOrUndefined(cashUp.at(0).get('objToSend'))) {// to run ajax call only when cashup is completed and new cashup is not created
                    console.log('invoked DeActivatePaymentMethods ajax call for deactivating payment methods after cashup')
                    new OB.DS.Process('com.openbravo.sharaf.retail.paymentmethods.service.DeActivatePaymentMethods').exec({
                        terminalId: OB.MobileApp.model.get('terminal').id,
                        businessDate: moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD')
                    }, function(data) {
                        if (data && data.exception) {
                            if (!OB.UTIL.isNullOrUndefined(data.exception.message)) {
                                console.log(data.exception.message);
                            }
                        } else {
                            var paymentMethod;
                            var paymentArrFromAjax = [];
                            var paymentArray = [];
                            if (Object.keys(data).length > 0) {
                                for (var i = 0; i < Object.keys(data).length; i++) {
                                    var p = Object.keys(data)[i];
                                    paymentArrFromAjax.push(p);
                                }
                            }
                            var pIndex = 0;
                            OB.MobileApp.model.get('payments').forEach(function(pay) {
                                if (paymentArrFromAjax.indexOf(pay.paymentMethod.id) === -1) {
                                    paymentArray[pIndex] = pay;
                                    pIndex++;
                                }
                            });
                            OB.MobileApp.model.attributes.payments = paymentArray;
                            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                        }
                    });
                } else {
                	//cashup is present already ... so, don't deactivate any payment
                	OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                }
                console.log('Deactivating payment method process completed');
            });
    	} else {
    		//business date is empty... dont attempt for DeActivatePaymentMethods AJAX call
    		OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    	}
    });
}());