package com.openbravo.sharaf.retail.paymentmethods.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.retail.posterminal.OBPOSAppCashup;
import org.openbravo.retail.posterminal.OBPOSAppPayment;
import org.openbravo.retail.posterminal.OBPOSApplications;

public class DeActivatePaymentMethods extends JSONProcessSimple{

	  private static Logger log = Logger.getLogger(DeActivatePaymentMethods.class);

	@Override
	public JSONObject exec(JSONObject jsonsent) throws JSONException,
			ServletException {
		String terminalID = null;
		String businessDate = null;
		OBPOSApplications posObj = null;
		JSONObject ResultantObj = new JSONObject();
		JSONObject result = new JSONObject();
		log.info("DeActivatePaymentMethods class invoked for de-activating payments");

		try {
			if(jsonsent.has("terminalId")){
				terminalID = jsonsent.getString("terminalId");
			}
			if(jsonsent.has("businessDate")){
				businessDate = jsonsent.getString("businessDate");
			}
			OBContext.setAdminMode(true);
			posObj = OBDal.getInstance().get(OBPOSApplications.class, terminalID);
		    ResultantObj = checkCashupProcessed(businessDate, posObj);
		} catch (Exception exc) {
		    log.error("Error in DeActivatePaymentMethods"+exc.getMessage());
		} finally {
			OBContext.restorePreviousMode();
	    }

		result.put("data", ResultantObj);
		result.put("status", 0);
		result.put("success", true);
		return result;
	}

	private JSONObject checkCashupProcessed(String businessDate, OBPOSApplications posObj) {
		JSONObject deactivatedPaymentObj = null;
		OBCriteria<OBPOSAppCashup> obcriteria = OBDal.getInstance().createCriteria(OBPOSAppCashup.class);
		obcriteria.add(Restrictions.eq(OBPOSAppCashup.PROPERTY_POSTERMINAL, posObj));
		obcriteria.addOrderBy(OBPOSAppCashup.PROPERTY_CASHUPDATE, false);

		OBPOSAppCashup obCriteriaObj = obcriteria.list().get(0);
		//JSONObject deactivatedPaymentObj = checkValidPayment(businessDate, terminalID, posObj);
		if (obCriteriaObj.isProcessed() == true){
			deactivatedPaymentObj = checkValidPayment(businessDate, posObj);
		}
		return deactivatedPaymentObj;
	}

	private JSONObject checkValidPayment(String businessDate, OBPOSApplications posObj) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		JSONObject inactivatedPaymentObj = new JSONObject();
		try {
			date = sdf.parse(businessDate);

			OBCriteria<OBPOSAppPayment> obCriteriaPayment = OBDal.getInstance().createCriteria(OBPOSAppPayment.class);
			obCriteriaPayment.add(Restrictions.eq(OBPOSAppPayment.PROPERTY_OBPOSAPPLICATIONS, posObj));

			for(OBPOSAppPayment payMethod : obCriteriaPayment.list()){
				if(payMethod.getCUSTSPMValidTill() != null){
					int out = payMethod.getCUSTSPMValidTill().compareTo(date);
					if (out < 0){
						payMethod.setActive(false);
						inactivatedPaymentObj.put(payMethod.getPaymentMethod().getId(), payMethod.getPaymentMethod().getName());
						OBDal.getInstance().save(payMethod);
					}
				}
			}
			OBDal.getInstance().commitAndClose();
		} catch (Exception e) {
			log.error("Error in CheckValidPayment"+e.getMessage());
		}
		return inactivatedPaymentObj;
	}

}